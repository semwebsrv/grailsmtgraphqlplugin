#!groovy

podTemplate(
  containers:[
    containerTemplate(name: 'jdk11',                image:'adoptopenjdk:11-jdk-openj9',   ttyEnabled:true, command:'cat')
  ],
  volumes: [
    // May need to look at this: https://stackoverflow.com/questions/48068027/how-to-configure-gradle-cache-when-running-jenkins-with-docker
    // if we get deadlock issues
    hostPathVolume(hostPath: '/var/lib/jenkins/.gradle', mountPath: '/home/gradle/.gradle'),
    hostPathVolume(hostPath: '/var/lib/jenkins/.gradledist', mountPath: '/root/.gradle')
  ])
{
  node(POD_LABEL) {
  
    // See https://www.jenkins.io/doc/pipeline/steps/pipeline-utility-steps/
    // https://www.jenkins.io/doc/pipeline/steps/
    // https://github.com/jenkinsci/nexus-artifact-uploader-plugin
  
    stage ('checkout') {
      checkout scm
    }
  
    stage ('check') {
      container('jdk11') {
        dir ( 'graphqlmt' ) {
          echo 'Hello, JDK'
          sh 'java -version'
          sh './gradlew --version'
        }
      }
    }
  
    stage ('build') {
      container('jdk11') {
        dir ( 'graphqlmt' ) {
          sh './gradlew --no-daemon --console=plain clean build generatePomFileForMavenPublication'
          sh 'ls ./build/libs'
        }
      }
    }
  
    stage ('publish') {
      dir ( 'graphqlmt' ) {
        def props = readProperties file: 'gradle.properties'
        def target_repository = null;
        println("Props: ${props}");
        def release_files = findFiles(glob: '**/graphqlmt-*.*.*.jar')
        println("Release Files: ${release_files}");
        if ( release_files.size() == 1 ) {
          // println("Release file : ${release_files[0].name}");
          if ( release_files[0].name.contains('SNAPSHOT') ) {
            target_repository='maven-snapshots';
          }
          else {
            target_repository='maven-releases';
          }
        }
  
        if ( target_repository != null ) {
          println("Publish ${release_files[0].path} with version ${props.appVersion} to ${target_repository}");
          nexusArtifactUploader(
            nexusVersion: 'nexus3',
            protocol: 'https',
            nexusUrl: 'nexus.semweb.co',
            groupId: 'com.semweb',
            version: props.appVersion,
            repository: target_repository,
            credentialsId: 'semweb-nexus',
            artifacts: [ [artifactId: 'graphqlmt', classifier: '', file: release_files[0].path, type: 'jar'],
                         [artifactId: 'graphqlmt', type: "pom", file: 'build/publications/maven/pom-default.xml'] ]
          )
        }
      }
    }
  }
}
